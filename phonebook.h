#ifndef PROG2_NHF_2019_V2_PHONEBOOK_H
#define PROG2_NHF_2019_V2_PHONEBOOK_H

#include <iostream>
#include "array.hpp"
#include "string.h"
#include "person.h"
#include "workingPerson.h"

class PhoneBook {
private:
    Array<Person*> people; // személy pointerek tömbje
public:
    void add(Person* p); // hozzá add egy személyt

    void remove(Person* p); // töröl egy személyt

    void remove(const char* nev); // töröl egy személyt név alapján

    void listOnlyNames(std::ostream& os) const; // kilistáza az összes személyt

    void list(std::ostream& os) const; // kilistáza az összes személyt (adatokkal)

    void list(std::ostream& os, const char* nev); // kilistáza az összes személyt (adatokkal), akinek a neve megegyezik a paramatérben kapottal
};

#endif