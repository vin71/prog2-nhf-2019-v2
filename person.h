#ifndef PROG2_NHF_2019_V2_PERSON_H
#define PROG2_NHF_2019_V2_PERSON_H

#include <ostream>
#include "string.h"

class Person {
private:
    String fullName; // teljes név
    String nickName; // becenév
    String address; // cím
    String mobilePhone; // mobil szám
public:
    Person(const char* f, const char* n, const char* a, const char* m); // konstruktor
    virtual ~Person(); // destruktor (virtuális)

    void printOnlyName(std::ostream& os) const; // kiírja a Person nevét
    virtual void print(std::ostream& os) const; // kiírja a Person adatait

    const String& getFullName() const { return fullName; } // teljes név getter
    const String& getNickName() const { return nickName; } // becenév getter
    const String& getAddress() const { return address; } // cím getter
    const String& getMobilePhone() const { return mobilePhone; } // mobil szám getter
};

#endif