#include "person.h"

Person::Person(const char* f, const char* n, const char* a, const char* m): fullName(String(f)), nickName(String(n)), address(String(a)), mobilePhone(String(m)) {}

Person::~Person() {}

void Person::printOnlyName(std::ostream& os) const {
     os << getFullName() << std::endl;
}

void Person::print(std::ostream& os) const {
     os << "Full Name: " << getFullName() << std::endl;
     os << "Nickname: " << getNickName() << std::endl;
     os <<  "Address: " << getAddress() << std::endl;
     os << "Mobile Phone: " << getMobilePhone() << std::endl;
     os << std::endl;
}