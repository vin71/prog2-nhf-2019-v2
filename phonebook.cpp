#include "phonebook.h"

void PhoneBook::add(Person* p) {
    people.add(p);
}

void PhoneBook::remove(Person* p) {
    people.remove(p);
}

void PhoneBook::remove(const char* nev) {
    for(size_t i = 0; i < people.len(); i++)
        if (!strcmp(people[i]->getFullName().strC(),nev))
            people.remove(people[i]);
}

void PhoneBook::listOnlyNames(std::ostream& os) const {
    for(size_t i = 0; i < people.len(); i++)
        os << people[i]->getFullName() << std::endl;
    os << std::endl;
}

void PhoneBook::list(std::ostream& os) const {
    for(size_t i = 0; i < people.len(); i++)
        people[i]->print(os);
}

void PhoneBook::list(std::ostream& os, const char* nev) {
    for(size_t i = 0; i < people.len(); i++)
        if (!strcmp(people[i]->getFullName().strC(),nev))
            people[i]->print(os);
}