#include "string.h"

String::String(const char* s) {
    size = strlen(s);
    str = new char[size+1];
    strcpy(str, s);
}

String::String(const String& rhs) {
    size = rhs.size;
    str = new char[size+1];
    strcpy(str, rhs.str);
}

String& String::operator=(const String& rhs) {
    if (this != &rhs){
        delete[] str;
        size = rhs.size;
        str = new char[size+1];
        strcpy(str, rhs.str);
    }
    return *this;
}

String::~String() {
    delete[] str;
}

String String::operator+(const String& rhs) const {
    String s;
    delete[] s.str;
    s.size = size + rhs.size;
    s.str = new char[s.size+1];
    strcpy(s.str, str);
    strcat(s.str, rhs.str);
    return s;
}

String String::operator+(char c) const {
    String s;
    delete[] s.str;
    s.size = size+1;
    s.str = new char[s.size+1];
    strcpy(s.str, str);
    char temp[] = {c,'\0'};
    strcat(s.str, temp);
    return s;
}

String& String::operator+=(const String& rhs) {
    return (*this = *this + rhs);
}

String& String::operator+=(char c) {
    char temp[] = {c,'\0'};
    return (*this = *this + String(temp));
}

std::ostream& operator<<(std::ostream& os, const String& rhs) {
    return (os << rhs.strC());
}

std::istream& operator>>(std::istream& is, String& rhs) {
    char c;
    String s;
    while (is.get(c) && c!='\n')
        s += c;
    rhs = s;
    return is;
}

std::istream& getline(std::istream& is, String& rhs) {
    char c;
    while (is.get(c) && c != '\n')
        rhs + c;
    return is;
}