#include "workingPerson.h"

workingPerson::workingPerson(const char* f, const char* n, const char* a, const char* m, const char* w): Person(f,n,a,m), workPhone(String(w)) {}

void workingPerson::print(std::ostream& os) const {
    os << "Full Name: " << getFullName() << std::endl;
    os << "Nickname: " << getNickName() << std::endl;
    os << "Address: " << getAddress() << std::endl;
    os << "Mobile Phone: " << getMobilePhone() << std::endl;
    os << "Work Phone: " << getWorkPhone() << std::endl;
    os << std::endl;
}