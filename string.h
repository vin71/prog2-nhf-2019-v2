#ifndef PROG2_NHF_2019_V2_STRING_H
#define PROG2_NHF_2019_V2_STRING_H

#include <iostream>
#include <cstring>

class String {
private:
    char* str; // karaktertömb pointer
    size_t size; // méret ( '/0' nem számít bele )
public:
    explicit String(const char* s = ""); // konstruktor
    String(const String& rhs); // másoló konstruktor
    String& operator=(const String& rhs); // egyenlőség operátor
    ~String(); // destruktor

    String operator+(const String& rhs) const; // összeadás operátor
    String operator+(char c) const; // összeadás operátor (const)
    String& operator+=(const String& rhs); // += operátor (String)
    String& operator+=(char c); // += operátor (char)

    char& operator[](size_t index) { return str[index]; } // indexelő operátor
    const char& operator[](size_t index) const { return str[index]; } // indexelő operátor (const)

    size_t len() const { return size; } // méret getter
    const char* strC() const { return str; } // karaktertömb getter
};

std::ostream& operator<<(std::ostream& os, const String& rhs); // kiíró operátor
std::istream& operator>>(std::istream& is, String& rhs); // beolvasó operátor

std::istream& getline(std::istream& is, String& rhs); // egész sort olvas

#endif