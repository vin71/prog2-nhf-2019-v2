#ifndef PROG2_NHF_2019_V2_WORKINGPERSON_H
#define PROG2_NHF_2019_V2_WORKINGPERSON_H

#include "person.h"
#include "string.h"

class workingPerson: public Person {
private:
    String workPhone; // munkahelyi szám
public:
    workingPerson(const char* f, const char* n, const char* a, const char* m, const char* w); // konstruktor

    void print(std::ostream& os) const; // kiírja a workingPerson adatait

    const String& getWorkPhone() const { return workPhone; } // munkahelyi szám getter
};

#endif