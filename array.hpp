#ifndef PROG2_NHF_2019_V2_ARRAY_HPP
#define PROG2_NHF_2019_V2_ARRAY_HPP

#include <cstddef>

template<typename T>
class Array {
private:
    T* data; // tömb pointer
    size_t size; // méret
public:
    explicit Array(size_t size = 0): data(new T[size]), size(size) {} // konstruktor

    Array(const Array<T>& rhs) {
        size = rhs.size;
        data = new T[size];
        for (size_t i = 0; i < size; i++)
            data[i] = rhs.data[i];
    } // másoló konstruktor

    Array<T>& operator=(const Array<T>& rhs) {
        if (this != &rhs) {
            delete[] data;
            size = rhs.size;
            data = new T[size];
            for (size_t i = 0; i < size; i++)
                data[i] = rhs.data[i];
        }
        return *this;
    } // egyenlőség operátor

    ~Array() {
        delete[] data;
    } // destruktor

    T& operator[](size_t index) {
        return data[index];
    } // indexelő operátor

    const T& operator[](size_t index) const {
        return data[index];
    } // indexelő operátor (const)

    size_t len() const {
        return size;
    } // méret getter

    T* dataP() const {
        return data;
    } // tömb pointer getter

    void add(const T& element) {
        T* newData = new T[size+1];
        for (size_t i = 0; i < size; i++)
            newData[i] = data[i];
        newData[size] = element;
        size++;
        delete[] data;
        data = newData;
    } // elem hozzáadása a tömbhöz

    void remove(T& element) {
        T* newData = new T[size-1];
        int cnt = 0;
        for (size_t i = 0; i < size; i++)
            if(data[i] != element)
                newData[cnt++] = data[i];
            else
                delete data[i];
        size--;
        delete[] data;
        data = newData;
    } // elem törlése a tömbből
};

#endif