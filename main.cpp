#include <iostream>
#include "phonebook.h"
#include "memtrace.h"

void test1(){
    std::cout << std::endl << "TEST1" << std::endl << "-----" << std::endl;

    // létrehozunk egy telefonkönyv példányt
    PhoneBook b1;

    // létrehozunk 3 személyt
    Person* p1 = new Person("Jennifer Dodson","Doisey","Mesa, 332 Dye Street","619-897-1058");
    Person* p2 = new workingPerson("Alen Holmes","Alen","Simpsonville, 2486 Brown Avenue","201-452-3540","603-748-4101");
    Person* p3 = new workingPerson("Reece Vargas","RickyBoy","San Diego, 3914 Hood Avenue","662-402-9196","917-577-5956");

    // hozzáadjuk a 3 személyt a telefonköyvhöz
    b1.add(p1);
    b1.add(p2);
    b1.add(p3);

    // kiírjuk az összes személyt
    b1.list(std::cout);

    b1.remove(p1);
    b1.remove(p2);
    b1.remove(p3);;
}

void test2(){
    std::cout << std::endl << "TEST2" << std::endl << "-----" << std::endl;

    // létrehozunk egy telefonkönyv példányt
    PhoneBook b1;

    // létrehozunk 3 személyt
    Person* p1 = new Person("Jennifer Dodson","Doisey","Mesa, 332 Dye Street","619-897-1058");
    Person* p2 = new workingPerson("Alen Holmes","Alen","Simpsonville, 2486 Brown Avenue","201-452-3540","603-748-4101");
    Person* p3 = new workingPerson("Reece Vargas","RickyBoy","San Diego, 3914 Hood Avenue","662-402-9196","917-577-5956");

    // hozzáadjuk a 3 személyt a telefonköyvhöz
    b1.add(p1);
    b1.add(p2);
    b1.add(p3);

    // kitöröljük az egyik elemet
    b1.remove(p2);

    // kiírjuk a neveket
    b1.listOnlyNames(std::cout);

    b1.remove(p1);
    b1.remove(p3);
}

void test3(){
    std::cout << std::endl << "TEST3" << std::endl << "-----" << std::endl;

    // létrehozunk egy telefonkönyv példányt
    PhoneBook b1;

    // létrehozunk 3 személyt
    Person* p1 = new Person("Jennifer Dodson","Doisey","Mesa, 332 Dye Street","619-897-1058");
    Person* p2 = new workingPerson("Alen Holmes","Alen","Simpsonville, 2486 Brown Avenue","201-452-3540","603-748-4101");
    Person* p3 = new workingPerson("Reece Vargas","RickyBoy","San Diego, 3914 Hood Avenue","662-402-9196","917-577-5956");

    // hozzáadjuk a 3 személyt a telefonköyvhöz
    b1.add(p1);
    b1.add(p2);
    b1.add(p3);

    // hozzáadunk egy személyt szab. bemenetről brolvassva az adatokat
    String fn, nn, a, mp;
    std::cout << "Add a person" << std::endl;
    std::cout << "Full Name: " << std::endl;
    std::cin >> fn;
    std::cout << "Nickname: " << std::endl;
    std::cin >> nn;
    std::cout << "Address: " << std::endl;
    std::cin >> a;
    std::cout << "Mobile Phone: " << std::endl;
    std::cin >> mp;
    Person* p4 = new Person(fn.strC(),nn.strC(),a.strC(),mp.strC());
    b1.add(p4);

    // kiírjuk az összes személyt
    b1.list(std::cout);

    b1.remove(p1);
    b1.remove(p2);
    b1.remove(p3);
    b1.remove(p4);
}

int main() {

    test1();
    test2();
    test3();

    return 0;
}